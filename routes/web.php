<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin/dashboard', 'DashboardController@index')->name('home');
Route::get('/admin/dashboard/media/add', 'MediaController@addScreen')->name('Add');
Route::get('/admin/dashboard/media/add-from-camera', 'MediaController@addFromCamera')->name('AddFromCamera');
Route::post('/media/insert', 'MediaController@insertMedia')->name('InsertMedia');
