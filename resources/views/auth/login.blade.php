@extends('layouts.app', ['body_class' => 'auth-login'])

@section('content')

    <section class="section section__title section__half">
        <h1>Admin Panel</h1>
        <span class="hr"></span>
        <p class="sub-title">Login into the admin panel</p>
    </section>

    <section class="section section__login section__half">

        <form action="{{ 'login' }}" class="login" method="post">

            {{ csrf_field() }}

            @if(!$errors->isEmpty())

            <div class="form-errors">

                @if ($errors->has('email'))
                    <span class="form-errors__span">
                        {{ $errors->first('email') }}
                    </span>
                @endif

                @if ($errors->has('password'))
                    <span class="form-errors__span">
                        {{ $errors->first('password') }}
                    </span>
                @endif
                
            </div>

            @endif
            
            <label for="username" class="form__label form__label--full">
                Username:
            </label>
            <input type="text" class="form__input form__input--full" name="username" />

            <label for="password" class="form__label form__label--full">
                Password:

            </label>
            <input type="password" class="form__input form__input--full" name="password">

            <label for="remember" class="form__label form__label--half">
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
            </label>

            <a class="btn btn-link" href="{{ route('password.request') }}">
                Forgot Your Password?
            </a>

            <button class="btn btn--login" type="submit">
                Let's go <i class="fa fa-arrow-right"></i>
            </button>

        </form>
        
    </section>

@endsection
