@extends('layouts.app', ['body_class' => 'auth-admin-login'])

@section('content')
<form action="{{ route('InsertMedia') }}" method="post" class="form add-media">
	{{ csrf_field() }}

{{-- 	@if(!$errors->isEmpty())

	<div class="form-errors">

	    @if ($errors->has('filmName'))
	        <span class="form-errors__span">
	            {{ $errors->first('filmName') }}
	        </span>
	    @endif

	    @if ($errors->has('password'))
	        <span class="form-errors__span">
	            {{ $errors->first('password') }}
	        </span>
	    @endif
	    
	</div>

	@endif --}}
	<label for="filmName">
		Film Name:
		<input type="text" name="filmName" value="{{ old('filmName') }}" id="filmName" class="validateme" data-validation-type="text" />
	</label>

	<label for="filmReleaseDate">
		Release Date:
		<input type="date" name="filmReleaseDate" id="filmReleaseDate" class="validateme" data-validation-type="date" value="{{ old('filmReleaseDate') }}" />
	</label>

	<label for="filmDescription">
		Film Description:
		<textarea name="filmDescription" id="filmDescription" cols="30" rows="10" class="validateme" data-validation-type="text">{{ old('filmDescription') }}</textarea>
	</label>

	<button type="submit">Add Media</button>
</form>
@endsection
