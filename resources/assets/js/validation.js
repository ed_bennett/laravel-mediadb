$.fn.validation = function() {

	// Store some properties to easily configure later
	var inputToValidate = $('form .validateme'),
		randId = function(length) {
			var text = '',
				possibleCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
				for(var i = 0; i < length; i++) {
					text += possibleCharacters.charAt(Math.floor(Math.random() * possibleCharacters.length));
				}

			return text;
		},
		r = randId(30);

		$(this).attr('data-uid', r);

		$(this + 'button[type="submit"]').attr('disabled', 'disabled');

}