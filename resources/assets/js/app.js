function randomID(length) {
	var text;
	var possibleCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for(var i = 0; i < length; i++) {
		text += possibleCharacters.charAt(Math.floor(Math.random() * possibleCharacters.length));
	}

	return text;
}

$.fn.validateMe = function() {

	// Store some properties to easily configure later
	var $this = $(this);
	var inputToValidate = $('.validateme', this);
	var rid = randomID(30);

	$(this).attr('data-uid', rid 	);
	$('button[type="submit"]', this).attr('disabled', 'disabled');

	$(inputToValidate).each(function() {

		$(this).on('keyup change', function(e) {

			var allLabels = $('label.success').length;
			var	allInputs = $(inputToValidate).length;

			if(allLabels == allInputs) {
				$('button[type="submit"]').removeAttr('disabled');
			} else {
				$('button[type="submit"]').attr('disabled', 'disabled');
			}

			var i = $.trim($(this).val());
			var message = $('.validation-message');
			var validationType = $(this).attr('data-validation-type');

			var key = (e.keyCode || e.which);
			var keyArray = [16, 20, 13, 9, 17, 18, 19];
			if($.inArray(key, keyArray) != -1) {
				return;
			}

			if(i.length > -1 && i.length < 1) {
				$(this).parent().removeClass('error success').addClass('required').children(message).html('<i class="fa fa-exclamation"></i>');
			}

			if(validationType === 'text') {

				var check = /^[a-zA-Z0-9\s]+$/;

				if(check.test(i) ) {
					$(this).parent().removeClass('error required').addClass('success').children(message).html('<i class="fa fa-check"></i>');
					return 'success';
				}

			} else if(validationType === 'email') {

				var check = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;

				if(check.test(i)) {
					$(this).parent().removeClass('error required').addClass('success').children(message).html('<i class="fa fa-check"></i>');
					return 'success';
				}

			} else if(validationType === 'date') {
					$(this).parent().removeClass('error required').addClass('success').children(message).html('<i class="fa fa-check"></i>');
					return 'success';

			}

			$(this).parent().removeClass('success required').addClass('error').children(message).html('<i class="fa fa-times"></i>');

			return 'fail';

		})

	});

	$(this).on('submit', function(e) {

		e.preventDefault();

		var $this = $(this);

		$.ajaxSetup({
			headers: {
			      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			type: $this.attr('method'),
			url: $this.attr('action'),
			data: $this.serialize(),
			dataType: 'JSON',
			success: function(data) {
				console.log(data);
				$this.append('<span class="loading" />');
				setTimeout(function() {
					$this.find('.loading').remove();
					$this.append($('<div class="form_message">'+data.message+'</div>'));
					$this.find($('button[type="submit"]')).attr('disabled', 'disabled');
				}, 2000);
			},
			error: function(xhr, status, data) {
				var error = $.parseJSON(xhr.responseText);
				var info = "<div class='form-errors'></div>";
				$(this).append(info);
				// console.log(data);
				// console.log(status);
				// console.log(xhr);
			}
		});

	});
};

// Let's do some validation
$('form').not('.login-form, .register-form').each(function(e) {
	$(this).validateMe();
});


// @prepros-prepend quagga.min.js

// Quagga.init({

//     inputStream : {
//     	  name: "Live",
//     	  type: "LiveStream",
//     	  constraints: {
//     	    width: 640,
//     	    height: 480,
//     	    facingMode: "environment",
//     	    deviceId: "7832475934759384534"
//     	  },
//     	  area: { // defines rectangle of the detection/localization area
//     	    top: "0%",    // top offset
//     	    right: "0%",  // right offset
//     	    left: "0%",   // left offset
//     	    bottom: "0%"  // bottom offset
//     	  },
//     	  singleChannel: false, // true: only the red color-channel is read
//       target: document.querySelector('#yourElement')    // Or '#yourElement' (optional)
//     },
//     decoder : {
//       readers : ["code_128_reader"]
//     }
//   }, function(err) {
//       if (err) {
//           console.log(err);
//           return
//       }
//       console.log("Initialization finished. Ready to start");
//       Quagga.start();
//   });