<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Media;
use Validator, Input, Redirect, Response, DB; 


class MediaController extends Controller
{
    /*
     * @var array Stores all data from form
     *
     */
    protected $formdata;

    /*
     * @var array Stores messages
     *
     */
    protected $messages;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     *
     * Show the add media screen
     * @return \Illuminate\Http\View
     *
     */
    public function addScreen()
    {
        return view('media.add');
    }

    public function addFromCamera()
    {
        return view('media.addFromCamera');
    }

    public function insertMedia(Request $request)
    {
        $this->formdata = $request->all();

        $validate = Validator::make($this->formdata, [
            'filmName' => [
                    'required',
                    'max:300',
                ],
        ]);

        $this->messages = [
            'success' => 'Media added successfully!'
        ];

        if($validate->fails()) :
            $errors = $validate->errors();
            $errors = json_decode($errors);

            return response()->json([
                'success'       =>      false,
                'message'       =>      $errors
            ], 422);
        endif;

        $media = new Media;
        $media->filmName = $this->formdata['filmName'];
        $media->filmReleaseDate = $this->formdata['filmReleaseDate'];
        $media->filmDescription = $this->formdata['filmDescription'];
        $media->save();

        return response()->json([
            'success' => true,
            'message' => $this->messages['success']            
        ], 200);
    }
}
